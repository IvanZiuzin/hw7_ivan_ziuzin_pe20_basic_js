"use strict"
// Этот код работает в современном режиме


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ  ------------------------
// ------< TASK#1 >------------------------------


// Реализовать функцию сильтра массива по указанному типу данных.
// Задание должно быть выполненно на чистом Javascript, без использования библиотек типа jQuery / React.

// Технические требования: 

// Написать функцию filterBy(), которая будет принимать 2 аргумента.
// Первый аргумент: массив, который содержит любые данные.
// Второй аргумент: тип данных.
// Функция должна возвращать новый массив, который будет содержать все данные, которые были переданы в аргумент,
// - за исключением тех, тип которых, был передан вторым аргументом.
// Т.е.: если передать массив ['hello', 'world', 23, '23', null] первым аргументом и вторым аргументом передать 'string', то:
// - функция вернет массив [23, null].


// АЛГОРИТМ РЕШЕНИЯ ЗАДАЧИ (от ДЗ№7): 

// 1/ Зададим следующую логику выполнения функции: 
// - Функция будет осуществлять фильтр торговых марок, типов велосипедов, размеров колес и пар значений, при этом: 
// - Торговая марка и тип велосипеда - это обязательно слово, тип данных - "Строка - String"
// - Размер колес - это обязательно числа, тип данных - "Число - Number"
// - Пара "Ключ - значение" - Тип/Марка велосипеда и размер колес - тип данных "Объект - Object"
// 2/ Применим знания по теме работы с массивами (тренировка навыков)



//  Start ot the TASK#1 ------------------------------------------------------------------------------
//  ХОД ВЫПОЛНЕНИЯ ЗАДАЧИ: 
console.log ('Task #1 < HW7 - filter-array > started  \u{1F680}');


// -----------------------------------------------------------------------------------------------
// 1/ Создаём основную функцию для выполнения задачи STEP#1: 
function filterBy(arr, typeofdata){
  return arr.filter(bikeList => typeof bikeList !== typeofdata);
} // Функция возвращает только тот тип данных который не передан вторым аргументом
  // Функция возвращает новый отфильтрованный массив  

// -----------------------------------------------------------------------------------------------
// 2 Объявляем 3 базовых массива STEP#2: ----------------------------
console.log(`\u{2705} STEP#2 Declaring 3 base arrays \u{1F6B2} :`);

// 2.1. Массив №1 - Содержит марки велосипедов:
let arrBikeTM = [ 
  'Trek Bicycle',
  'Giant Bicycle', 
  'Specialized', 
  'Scott Bicycle', 
  'Cannondale Bicycle', 
  'Merida', 
  'Schwinn', 
  'Bianchi', 
  'Kellys',
];
console.log(`\u{1F6A9} 1-st Array 'arrBikeTM':`, arrBikeTM);
console.log(`Array's length:`, arrBikeTM.length);

// 2.2. Массив №2  - содержит размеры колес велосипедов
const arrBikeWheels = [12, 16, 20, 20, 24, 24, 26, 26, 27.5, 27.5, 29, 29];
console.log(`\u{1F6A9} 2-nd Array 'arrBikeWheels':`, arrBikeWheels);
console.log(`Array's length:`, arrBikeWheels.length);

// 2.2. Массив №3 - содержит типы велосипедов: 

const arrBikeTypes = ['MTB', 'Road bike', 'Hybrid bike', 'BMX', 'Touring bike', 'Folding bike'];
console.log(`\u{1F6A9} 3-rd Array 'arrBikeTypes':`, arrBikeTypes);
console.log(`Array's length:`, arrBikeTypes.length);



// -----------------------------------------------------------------------------------------------
// 3. Несколько операций с массивами STEP#3: ----------------------------
console.log(`\u{2705} STEP#3 Operations whith arrays:`);

// 3.1. Добавляем три дополнительных элемента  в конец массива (метод push()):
console.log(`- The arr.push() method:`); 
arrBikeTM.push('Cube', 'GT', 'GHOST-Bikes');
console.log(`Array's length:`, arrBikeTM.length);

// 3.2. Объединяем массивы №1 и №3 (метод concat()):
console.log(`- The arr.concat() method:`); 

let arrBikeCompilation = arrBikeTM.concat(arrBikeTypes);
console.log(arrBikeCompilation);

// 3.3. Проверяем является ли массив массивом при помощи метода Array.isArray():
console.log(`\u{2611} Is the object array?\nAnswer:${Array.isArray(arrBikeCompilation)}!`);

// 3.4. Определяем индекс первого элемента - тип велосипеда (метод indexOf()):
console.log(`- The arr.indexOf() method:`, arrBikeCompilation.indexOf('MTB')); 

// 3.5. Применяем метод splice() для удаления элекментов из массива:
console.log(`- The arr.splice() method:`); 

let removedElements = arrBikeCompilation.splice(arrBikeCompilation.indexOf('MTB'));
console.log(`- After splice():`,arrBikeCompilation);
console.log(`- Removed elements:`, removedElements);



// -----------------------------------------------------------------------------------------------
// 4. Применение встроенных объектов Map и Set STEP#4 ----------------------------
console.log(`\u{2705} STEP#4.1. Creating new Map():`);


// ------------------------------------------------------------------------
//4.2. Создаём объект при помощи встроенного метода Map() - коллекция "ключ - значение":
// ключ - ТМ велосипеда, значение - размер колес / - велосипед в сборе:

const objBikeАssembled = new Map();

for (let bike = 0; bike < arrBikeTM.length; bike++) {
  objBikeАssembled.set(arrBikeTM[bike], arrBikeWheels[bike]);  
} // проходим циклом for и присваивам ключу - значение
console.log(objBikeАssembled);
console.log(`\u{2611} Is the object array?\nAnswer:${Array.isArray(objBikeАssembled)}!`); // проверям  - это будет объект, а нaм нужен массив

//4.2.1. Преобразуем объект в массив при помощи метода object.entries():
// - получаем массив массивов, каждый из которых - пара "ключ - значение"
console.log(`- The Array.from & object.entries() methods:`); 

const arrBikeАssembled = Array.from(objBikeАssembled.entries());
console.log(arrBikeАssembled.sort()); // сразу сортируем по алфавиту

//4.2.2. Объединяем массивы objBikeАssembled и arrBikeTypes: 
let arrFinalBikeList = arrBikeАssembled.concat(arrBikeTypes);

//--------// Этот  массив будем тестировать в функции:
console.log(`\u{1F6A9} The array "arrFinalBikeList" for filterBy() testing - contains strings & objects`, arrFinalBikeList.sort());



// ------------------------------------------------------------------------
// 4.3. Создаём объект при помощи встроенного метода Set() - коллекция уникальных значений любого типа:
// - сключает повторения:
console.log(`\u{2705} STEP#4.2. Creating new Set():`);

// 4.3.1. Добаввляем в массив №3 дублирующиеся элементы при помощи метода push():
arrBikeTypes.push('BMX', 'Touring bike', 'Folding bike');
console.log(`The array whith duplicate elements:`, arrBikeTypes);

// 4.3.2. Создаем объект при помощи Set():
const objBikesSetList = new Set([...arrBikeTypes, ...arrBikeWheels]); // используем оператор распростанения spread

console.log(objBikesSetList);
console.log(`\u{2611} Is the object array?\nAnswer:${Array.isArray(objBikesSetList)}!`); // это будет объект, а нам нужем массив

// 4.3.3. Преобразуем в массив и применим метод arr.map():
console.log(`- The Array.from & arr.map() methods:`); 
const arrBikesSetList = Array.from(objBikesSetList).map((bike) => {return {'bike item - \u{1F6B2}': bike}});

console.log(arrBikesSetList.sort()); // получаем массив где элементы - объекты 
console.log(`\u{2611} Is the object array?\nAnswer:${Array.isArray(arrBikesSetList)}!`); // проверяем является ли массивом
console.log(typeof arrBikesSetList[3], arrBikesSetList[3]); // проверяем тип данных элементов массива

// 4.3.4. Добавляем несколько элементов в массив: 
arrBikesSetList.push(`\u{1F6B4}`,`\u{1F6B4}`,`\u{1F6B4}`);
console.log(typeof arrBikesSetList[14], arrBikesSetList[14]); 

//--------// Этот  массив будем тестировать в функции:
console.log(`\u{1F6A9} The array "arrBikesSetList" for filterBy() testing - contains strings & objects`, arrBikesSetList.sort());




// -----------------------------------------------------------------------------------------------
// 5. Создаём массив с тремя типами данных STEP#5:
console.log(`\u{2705} STEP#5 Creating array -  arr.slice method:`);

// из частей существующих массивов:
let slice1 = arrBikeWheels.slice(0,5);
let slice2 = arrBikesSetList.slice(10,15);
let arrWheelsAndRiders = slice1.concat(slice2);

console.log(`\u{1F6A9} The array "arrwheelsAndRiders" for filterBy() testing - contains strings, numbers & objects`, arrWheelsAndRiders.sort());


// -----------------------------------------------------------------------------------------------
// 6. ------< ПРИМЕНЕНИЕ ФУНКЦИИ STEP#6: >------------------------------

console.log(`\u{2705} STEP#6 function filterBy() testing:`);
// test 1
console.log(`\u{1F3C1} Test 1 arrFinalBikeList:`);
console.log(`excluding objects`, filterBy(arrFinalBikeList, 'object'));
console.log(`excluding strings`,filterBy(arrFinalBikeList, 'string'));

// test 2
console.log(`\u{1F3C1} Test 2 arrBikesSetList:`);
console.log(`excluding objects`, filterBy(arrBikesSetList, 'object'));
console.log(`excluding strings`, filterBy(arrBikesSetList, 'string'));

// test 3
console.log(`\u{1F3C1} Test 3 arrWheelsAndRiders:`);
console.log(`excluding objects`, filterBy(arrWheelsAndRiders, 'object'));
console.log(`excluding strings`, filterBy(arrWheelsAndRiders, 'string'));
console.log(`excluding numbers`, filterBy(arrWheelsAndRiders, 'number'));

// ------< TASK#1 >------ЗАВЕРШЕН------------------------
console.log ('Task #1 < HW7 - filter-array > finished!:) \u{1F60E} \u{1F3C1}')


